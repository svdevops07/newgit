# GITLAB PROJECT WITH VOLUME



## Install and configure GitLab
```
    sudo apt-get update
    sudo apt-get install -y curl openssh-server ca-certificates tzdata perl

    sudo apt-get install -y postfix
```

## Add the GitLab package repository and install the package
```
    curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script. deb.sh | sudo bash
    sudo EXTERNAL_URL="http://localhost" apt-get install gitlab-ee
```

## Install Docker + change ownership
```
    sudo apt-get install docker docker-compose
    sudo usermod -a -G docker $USER
```

## Create new directory for the project
```
mkdir gitproject
cd gitproject
```
Then create config-file, html-file and dockerfile.


## Create image and build the project
```
    docker build -t gitproject/nginxtest .
    docker run --name web -p 85:80 -d gitproject/nginxtest
```

## Connect local directory and GitLab directory and add created files for html-site and dockerfile
```
    git config --global user.name "gitproject"
    git config --global user.email "gitproject@git.project"

    git init
    git add .
    git commit -m "First commit"
    git remote add origin http://localhost/gitproject/gitlabproject.git
    git push -u origin --all
```

